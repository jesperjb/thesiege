using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {
	
	public GameObject Opponent;
	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag("Player") as GameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void AddOpponent() {
		Vector3 playerPos = player.transform.position;
		Vector3 pos = this.transform.position;
		GameObject opp = Instantiate(Opponent, pos, Quaternion.LookRotation(playerPos)) as GameObject;
		opp.GetComponent<Opponent>().SetTarget(player);
		
		//opp.transform.position = new Vector3(0, 14, 0);
	}
}
