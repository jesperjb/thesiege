using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	
	public static float score = 0;

	public static void AddPoints(float gain) {
		score += gain;
	}
	
	public static float GetPoints() {
		return score;
	}
	
	void OnGUI () {
	    GUILayout.BeginArea(new Rect(10, 10, Screen.width / 4, Screen.height / 4));
	    GUILayout.Box("Score: " + score);
	    GUILayout.EndArea();
	}
}
