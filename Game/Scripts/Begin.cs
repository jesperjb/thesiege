using UnityEngine;
using System.Collections;

public class Begin : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI() {
		GUILayout.BeginArea(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 100, 300, 200));
    	GUILayout.TextArea("Welcome to Zombie Siege.\n" +
    		"You have been trapped between a number of warehouses with zombies on all sides.\n" +
    		"All you can do is take as meny of them with you before they kill you.");
		GUILayout.Box("Use the mouse to turn and space to shoot.");
		if(GUILayout.Button("Begin")) {
			Application.LoadLevel("Siege scene");
		}
    	GUILayout.EndArea();
	}
}
