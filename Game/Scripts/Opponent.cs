using UnityEngine;
using System.Collections;

public class Opponent : MonoBehaviour {
	
	private GameObject player;
	public float speed = 0.1f;
	public float maxDistance = 1f;
	public AudioSource hitSound;
	
	public bool canMove = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(player && canMove) {
			Vector3 pos = player.transform.position;
			Debug.Log(Vector3.Distance(transform.position, pos) + " : " + maxDistance);
			if(Vector3.Distance(transform.position, pos) >= maxDistance) {
				this.transform.LookAt(pos);
				this.transform.position = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);
			} else {
				player.GetComponent<Player>().Die();
				AIController.StopSpawns();
			}
		}
	}
	
	void OnTriggerEnter(Collider other) {
		if(other.tag == "Projectile") {
			hitSound.Play();
			DestroyObject(gameObject);
		}
	}
	
	public void SetTarget(GameObject tgt) {
		player = tgt;
	}
	
	public void OnHit(float damage) {
		Die();
	}
	
	public void Die() {
		hitSound.Play();
		DestroyObject(gameObject);
	}
	
	public void Stop() {
		canMove = false;
	}
}
