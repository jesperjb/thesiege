using UnityEngine;
using System.Collections;

public class Minimap : MonoBehaviour {

	public Texture2D arrow; // drag the arrow texture here
	Rect rArrow = new Rect(Screen.width - 163f, 49f, 110, 110); // rectangle where to draw the arrow

	void OnGUI(){
	  Matrix4x4 svMat = GUI.matrix; // save the current GUI matrix
	  // find the pivot and rotation angle:
	  Vector2 pivot = rArrow.center; // get the arrow center point
	  float ang = transform.eulerAngles.y; // get the character angle
	  // set GUI matrix to rotate ang degrees around the pivot:
	  GUIUtility.RotateAroundPivot(ang, pivot);
	  GUI.DrawTexture(rArrow, arrow); // draw the arrow
	  GUI.matrix = svMat; // restore the matrix to not affect other GUI items
	}
}
