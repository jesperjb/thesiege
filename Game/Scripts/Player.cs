using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	// public
	public GameObject muzzle;
    public GameObject projPrefab;
    public float projMuzzleVelocity; // in metres per second
    public float RateOfFire;
    public float Inaccuracy;
	public AudioSource gunSound;
	public Texture reticle;
	public GameObject rayBase;
	public bool canShoot = true;
	public bool dead = false;
 
    // private
    private float fireTimer;

	// Use this for initialization
	void Start () {
		fireTimer = Time.time + RateOfFire;
		Screen.showCursor = false;
		Screen.lockCursor = true;
		AIController.canSpawn = true;
		Score.score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonUp("Fire1") && canShoot) {
			Shoot();
		}
	}
	
	void Shoot() {
		//Debug.DrawLine(muzzle.transform.position, muzzle.transform.position + muzzle.transform.forward, Color.red);
        if (Time.time > fireTimer)
        {

            fireTimer = Time.time + RateOfFire;
			
			// Raycast
			Ray ray = new Ray(rayBase.transform.position, muzzle.transform.forward);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 1000)) {
				Debug.Log("Hit" + hit.transform.root);
				hit.transform.root.SendMessage("OnHit", 5, SendMessageOptions.DontRequireReceiver);
				Score.AddPoints(5);
			}
			Debug.Log(gunSound);
			gunSound.Play();
        }
        else
            return;
	}
	
	void OnGUI(){
	    //GUI.Box(new Rect(Screen.width/2,Screen.height/2, 10, 10), "");
		GUI.DrawTexture(new Rect(Screen.width/2-(reticle.width*0.5f), Screen.height/2-(reticle.height*0.5f), reticle.width, reticle.height), reticle);
		if (Time.time < fireTimer) {
			GUILayout.BeginArea(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 40, 200, 30));
	    	GUILayout.Box("Shoot again in: " + (Mathf.Round((fireTimer - Time.time) * 100) / 100));
	    	GUILayout.EndArea();
		}
		if(dead) {
			GUILayout.BeginArea(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 200));
			float score = Score.GetPoints();
	    	GUILayout.TextArea("You have died!\n\nYou managed to get: " + score.ToString());
			if(GUILayout.Button("Replay?")) {
				Application.LoadLevel(Application.loadedLevel);
			}
	    	GUILayout.EndArea();
		}
	 }
	
	public void Die() {
		Debug.Log("You have died");
		canShoot = false;
		dead = true;
		Camera.mainCamera.GetComponent<MouseLook>().canTurn = false;
		Screen.showCursor = true;
		Screen.lockCursor = false;
	}
}
