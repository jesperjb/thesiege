using UnityEngine;
using System.Collections;

public class AIController : MonoBehaviour {
	
	public float spawnTime = 5f;
	public float spawnsBeforeDecrease = 5f;
	public float spawnTimeDecrease = 0.2f;
	public float spawnCount = 0f;
	private float timeLeft;
	private SpawnPoint[] spawnPoints;
	public static bool canSpawn = true;

	// Use this for initialization
	void Start () {
		timeLeft = spawnTime;
		spawnPoints = gameObject.GetComponentsInChildren<SpawnPoint>();
	}
	
	// Update is called once per frame
	void Update () {
		timeLeft -= Time.deltaTime;
		if(timeLeft <= 0 && AIController.canSpawn) {
			// Spawn enemy
			float randSpawnPoint = Random.Range(0, spawnPoints.Length - 1);
			spawnPoints[(int) randSpawnPoint].AddOpponent();
			timeLeft = spawnTime;
			// Decease spawn time to increase difficulty
			spawnCount++;
			if(spawnCount > spawnsBeforeDecrease && spawnTime > 0) {
				spawnTime -= spawnTimeDecrease;
				spawnCount = 0;
			}
		}
	}
	
	public static void StopSpawns() {
		GameObject[] opponents = GameObject.FindGameObjectsWithTag("Enemy");
		foreach(GameObject opp in opponents) {
			opp.GetComponent<Opponent>().Stop();
		}
		AIController.canSpawn = false;
	}
}
